# My Bank Account

# Contexte
L'objectif de l'application est de pouvoir gérer son budget via une application bancaire. Elle doit permettre de gérer les entrées et sorties d'argent, de les catégoriser (loisir, famille, loyer, prêt immo, etc..), ainsi que de geler une entrée si nécessaire. 

L'application doit aussi permettre d'éditer des statistiques mensuelles sur le pourcentage des dépenses par type, l'évolution au fil du temps, etc.

## Conception

J'ai tous d'abord commencé par créer un diagramme de cas d'utilisation, afin de déterminer les besoins de mon application et définir ses fonctionnalités.

Il pourra y avoir plusieurs utilisateurs sur l'application, mais chacun aura accès aux mêmes fonctionnalités. J'ai choisi des couleurs différentes sur le diagramme afin de mieux repérer les différents sujets.

Ensuite, j'ai créé un diagramme servant à définir le modèle de données. Celui-ci permet de définir quels seront les différentes table à créer ainsi que leur contenu, et les relations entre elles. 
Tout part d'un User qui pourra créer plusieurs BankAccount, et donc il faudra une table de liaison étant donnée la cardinalité en many to many. 
Ce BankAccount permettra d'éditer des statistiques, et de gérer des Expenses, qui elles-mêmes pourront être catégorisées. Une dépense n'aura qu'une catégorie, tandis qu'une catégorie pourra appartenir à plusieurs dépenses. De même qu'un compte bancaire pourra détenir plusieurs expense, tandis qu'une expense ne sera lié qu'à un seul compte.

Finalement, j'ai complété le modèle de données afin d'en faire un diagramme de classe complet, comprenant les différentes interfaces contenant les méthodes qui seront implémentées dans les différentes classes.